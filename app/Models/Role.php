<?php

namespace App\Models;

use Zizaco\Entrust\EntrustRole;

class Role extends EntrustRole
{
  public function getRoleByName($name){
    return $this::where('name', $name)->first();
  }
}
