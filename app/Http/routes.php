<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'HomeController@index');

Route::auth();
Route::get('/config/roles', 'ConfigController@roles');

Route::group(['prefix' => 'admin', 'middleware' => ['role:admin|owner']], function() {
  Route::get('/users', 'UserController@index');
  Route::get('/user/assign-role/{id}/{role}', 'UserController@assignRole');

  Route::get('/articles', 'ArticleController@index');
  Route::post('/article', 'ArticleController@store');
  Route::get('/article/create', 'ArticleController@create');
  Route::get('/article/edit/{id}', 'ArticleController@edit');
  Route::put('/article', 'ArticleController@update');
  Route::get('/article/visible/{id}', 'ArticleController@visible');

  Route::get('/members', 'MemberController@index');
  Route::post('/member', 'MemberController@store');
  Route::get('/member/create', 'MemberController@create');
  Route::get('/member/edit/{id}', 'MemberController@edit');
  Route::put('/member', 'MemberController@update');
  Route::get('/member/active/{id}', 'MemberController@active');

  Route::get('/galleries', 'GalleryController@index');
  Route::post('/gallery', 'GalleryController@store');
  Route::get('/gallery/create', 'GalleryController@create');
  Route::get('/gallery/edit/{id}', 'GalleryController@edit');
  Route::put('/gallery', 'GalleryController@update');
  Route::get('/gallery/active/{id}', 'GalleryController@active');
});
