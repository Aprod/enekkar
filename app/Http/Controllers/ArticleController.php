<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\Article;
use Auth;
use App\User;

class ArticleController extends Controller
{
  public function index(){
    $articles = Article::all();

    return view('admin.articles', ['articles' => $articles]);
  }

  public function store(Request $request){
    $article = new Article();
    $article->user_id = Auth::user()->id;
    $article->section_id = 1;
    $article->title = $request->title;
    $article->content = $request->content;
    $article->visible = false;
    $article->save();

    return redirect('admin/articles');
  }

  public function create(Request $request){
    $article = new Article();
    return view('admin.article', ['article' => $article]);
  }

  public function edit($id){
    $article = Article::find($id);

    return view('admin.article', ['article' => $article]);
  }

  public function update(Request $request){
    $article = Article::find($request->id);
    if ($article) {
      $article->user_id = Auth::user()->id;
      $article->section_id = 1;
      $article->title = $request->title;
      $article->content = $request->content;
      $article->save();
    }

    return redirect('admin/articles');
  }

  public function visible($id){
    $article = Article::find($id);
    if ($article){
      $article->visible = !$article->visible;
      $article->save();
    }

    return redirect('admin/articles');
  }
}
