<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\User;
use App\Models\Role;
use Auth;
use Redirect;

class UserController extends Controller
{
    public function index(){
      $users = User::all();

      return view('admin.users', ['users' => $users]);
    }

    public function assignRole($id, $role){
      if ($role != 'owner' || Auth::user()->hasRole('owner')){
        $user = User::find($id);
        if (!$user->hasRole($role)){
          $user->attachRole($this->findRoleByName($role));
          if ($user->hasRole('inactive')){
            $user->detachRole($this->findRoleByName('inactive'));
          }
        } else {
          $user->detachRole($this->findRoleByName($role));
          if (!$user->hasRole('owner') && !$user->hasRole('admin') && !$user->hasRole('member')){
            $user->attachRole($this->findRoleByName('inactive'));
          }
        }
      }
      return redirect()->back();
    }

    protected function findRoleByName($name){
      return \App\Models\Role::where('name', $name)->first();
    }

}
