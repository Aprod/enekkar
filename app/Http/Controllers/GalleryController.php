<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\Gallery;
use Auth;
use App\Models\Role;

class GalleryController extends Controller
{
  public function index(){
    $galleries = Gallery::all();

    return view('admin.galleries', ['galleries' => $galleries]);
  }

  public function store(Request $request){
    $gallery = new Gallery();
    $gallery->user_id = Auth::user()->id;
    $role = $this->findRoleByName($request->role);
    $role ? $gallery->role_id = $role->id : $gallery->role_id = 0;
    $gallery->title = $request->title;
    $gallery->description = $request->description;
    $gallery->cover = $request->cover;
    $gallery->images = json_encode(explode(';', $request->images));
    $gallery->active = false;
    $gallery->save();

    return redirect('admin/galleries');
  }

  public function create(Request $request){
    $gallery = new Gallery();
    return view('admin.gallery', ['gallery' => $gallery]);
  }

  public function edit($id){
    $gallery = Gallery::find($id);
    $images= '';
    foreach (json_decode($gallery->images) as $image){
      $images .= $image . ';';
    }
    $gallery->images = $images;
    return view('admin.gallery', ['gallery' => $gallery]);
  }

  public function update(Request $request){
    $gallery = Gallery::find($request->id);
    if ($gallery) {
      $gallery->user_id = Auth::user()->id;
      $role = $this->findRoleByName($request->role);
      $role ? $gallery->role_id = $role->id : $gallery->role_id = 0;
      $gallery->title = $request->title;
      $gallery->description = $request->description;
      $gallery->cover = $request->cover;
      $gallery->images = json_encode(explode(';', $request->images));
      $gallery->save();
    }

    return redirect('admin/galleries');
  }

  public function active($id){
    $gallery = Gallery::find($id);
    if ($gallery){
      $gallery->active = !$gallery->active;
      $gallery->save();
    }

    return redirect('admin/galleries');
  }

  protected function findRoleByName($name){
    return \App\Models\Role::where('name', $name)->first();
  }
}
