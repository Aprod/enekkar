@extends('layouts.app')

@section('content')
<div class="col-md-8 col-md-offset-2">
  <div class="row">
    <div class="panel panel-default">
      <div class="panel-heading">Users</div>
        <div class="panel-body">
        <table class="table">
          <thead>
            <tr>
              <th>Name</th>
              <th>Email</th>
              <th>ID</th>
              <th>Member</th>
              <th>Admin</th>
              <th>Owner</th>
              <th>Inactive</th>
            </tr>
          </thead>
          <tbody>
            @foreach($users as $user)
            <tr>
                <td>{{ $user->name }}</td>
                <td>{{ $user->email }}</td>
                <td>{{ $user->id }}</td>
                <td>
                  @if ($user->hasRole('member'))
                    <a href="{{ URL::to('admin/user/assign-role/' . $user->id . '/member') }}"><i class="fa fa-check-circle"></i></a>
                  @else
                    <a href="{{ URL::to('admin/user/assign-role/' . $user->id . '/member') }}"><i class="fa fa-ban"></i></a>
                  @endif
                </td>
                <td>
                  @if ($user->hasRole('admin'))
                    <a href="{{ URL::to('admin/user/assign-role/' . $user->id . '/admin') }}"><i class="fa fa-check-circle"></i></a>
                  @else
                    <a href="{{ URL::to('admin/user/assign-role/' . $user->id . '/admin') }}"><i class="fa fa-ban"></i></a>
                  @endif
                </td>
                <td>
                  @if ($user->hasRole('owner'))
                    <a href="{{ URL::to('admin/user/assign-role/' . $user->id . '/owner') }}"><i class="fa fa-check-circle"></i></a>
                  @else
                    <a href="{{ URL::to('admin/user/assign-role/' . $user->id . '/owner') }}"><i class="fa fa-ban"></i></a>
                  @endif
                </td>
                <td>
                  @if ($user->hasRole('inactive'))
                    <i class="fa fa-check-circle"></i>
                  @else
                    <i class="fa fa-ban"></i>
                  @endif
                </td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
@endsection
