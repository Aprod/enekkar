@extends('layouts.app')

@section('content')
<div class="col-md-8 col-md-offset-2">
  <div class="row">
    <div class="panel panel-default">
      <div class="panel-heading">
        <div class="col-md-3">Articles</div>
        <div class="col-md-2 col-md-offset-7"><a href="/admin/article/create" class="btn btn-default"><i class="fa fa-plus-circle"></i> Add new</a></div>
        <div class="clearfix"></div>
      </div>
        <div class="panel-body">
        <table class="table">
          <thead>
            <tr>
              <th>Title</th>
              <th>Created at</th>
              <th>Updated at</th>
              <th>Section id</th>
              <th>Author</th>
              <th>Visible</th>
              <th>Edit</th>
            </tr>
          </thead>
          <tbody>
            @foreach($articles as $article)
            <tr>
                <td>{{ $article->title }}</td>
                <td>{{ $article->created_at }}</td>
                <td>{{ $article->updated_at }}</td>
                <td>{{ $article->section_id }}</td>
                <td>{{ $article->user->name }}</td>
                <td>
                  @if ($article->visible)
                    <a href="{{ URL::to('admin/article/visible/' . $article->id) }}"><i class="fa fa-check-circle"></i></a>
                  @else
                    <a href="{{ URL::to('admin/article/visible/' . $article->id) }}"><i class="fa fa-ban"></i></a>
                  @endif
                </td>
                <td>
                    <a href="{{ URL::to('admin/article/edit/' . $article->id) }}"><i class="fa fa-pencil"></i></a>
                </td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
@endsection
