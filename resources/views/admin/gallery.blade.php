@extends('layouts.app')

@section('content')
<div class="col-md-8 col-md-offset-2">
  <div class="row">
    <div class="panel panel-default">
      <div class="panel-heading">
        Gallery
      </div>
        <div class="panel-body">
          <form role="form" action="/admin/gallery" method="POST">
              @if (!empty($gallery->id))
              <input type="hidden" name="_method" value="PUT">
              <input type="hidden" name="id" value="{{ $gallery->id }}">
              @endif
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
              <div class="form-group">
                <label for="title">Title:</label>
                <input type="text" name="title" class="form-control" value="{{ $gallery->title }}">
              </div>
              <div class="form-group">
                <label for="description">Description:</label>
                <input type="text" name="description" class="form-control" value="{{ $gallery->description }}">
              </div>
              <div class="form-group">
                <label for="cover">Cover photo:</label>
                <input type="text" name="cover" class="form-control" value="{{ $gallery->cover }}">
              </div>
              <div class="form-group">
                <label for="images">Images:</label>
                <textarea name="images" class="form-control" rows=10>{{ $gallery->images }}</textarea>
              </div>
              <button type="submit" class="btn btn-default">Submit</button>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
