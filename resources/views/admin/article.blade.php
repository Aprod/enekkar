@extends('layouts.app')

@section('content')
<div class="col-md-8 col-md-offset-2">
  <div class="row">
    <div class="panel panel-default">
      <div class="panel-heading">
        Article
      </div>
        <div class="panel-body">
          <form role="form" action="/admin/article" method="POST">
              @if (!empty($article->id))
              <input type="hidden" name="_method" value="PUT">
              <input type="hidden" name="id" value="{{ $article->id }}">
              @endif
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
              <div class="form-group">
                <label for="title">Title:</label>
                <input type="text" name="title" class="form-control" value="{{ $article->title }}">
              </div>
              <div class="form-group">
                <label for="content">Content:</label>
                <textarea name="content" class="form-control" rows=15>{{ $article->content }}</textarea>
              </div>
              <button type="submit" class="btn btn-default">Submit</button>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
