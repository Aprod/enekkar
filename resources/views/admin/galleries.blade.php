@extends('layouts.app')

@section('content')
<div class="col-md-8 col-md-offset-2">
  <div class="row">
    <div class="panel panel-default">
      <div class="panel-heading">
        <div class="col-md-3">Galleries</div>
        <div class="col-md-2 col-md-offset-7"><a href="/admin/gallery/create" class="btn btn-default"><i class="fa fa-plus-circle"></i> Add new</a></div>
        <div class="clearfix"></div>
      </div>
        <div class="panel-body">
        <table class="table">
          <thead>
            <tr>
              <th>Title</th>
              <th>Description</th>
              <th>Created at</th>
              <th>Cover image</th>
              <th>Role</th>
              <th>Active</th>
              <th>Edit</th>
            </tr>
          </thead>
          <tbody>
            @foreach($galleries as $gallery)
            <tr>
                <td>{{ $gallery->title }}</td>
                <td>{{ $gallery->description }}</td>
                <td>{{ $gallery->created_at }}</td>
                <td>{{ $gallery->cover }}</td>
                <td>{{ $gallery->role }}</td>
                <td>
                  @if ($gallery->active)
                    <a href="{{ URL::to('admin/gallery/active/' . $gallery->id) }}"><i class="fa fa-check-circle"></i></a>
                  @else
                    <a href="{{ URL::to('admin/gallery/active/' . $gallery->id) }}"><i class="fa fa-ban"></i></a>
                  @endif
                </td>
                <td>
                    <a href="{{ URL::to('admin/gallery/edit/' . $gallery->id) }}"><i class="fa fa-pencil"></i></a>
                </td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
@endsection
