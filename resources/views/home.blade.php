<!DOCTYPE html>
<html lang="hu">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title>Pázmány ITK Énekkar</title>

  <!-- Meta Description -->
      <meta name="description" content="PPKE ITK Énekkar Honlap">
      <meta name="keywords" content="PPKE, Pázmány, ITK, énekkar, kórus, honlap, egyetem, Bércesné">
      <meta name="author" content="Dobránszky Márk, Illés Apród">

  <!-- Bootstrap -->
  <link href="css/bootstrap.min.css" rel="stylesheet">
  <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
  <!-- Include all compiled plugins (below), or include individual files as needed -->
  <script src="js/bootstrap.min.js"></script>

  <!-- Bootstrap Gallery -->
      <link rel="stylesheet" href="css/blueimp-gallery.min.css">

  <!-- JQuery -->
      <script src="js/jquery.js"></script>
  <!-- My Style -->
      <link rel="stylesheet" href="css/style.css">

  <!-- Fonts -->
  <link href="http://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet" type="text/css">
  <link href="http://fonts.googleapis.com/css?family=Lato" rel="stylesheet" type="text/css">

  <!-- Icons -->
  <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css">

</head>

<body id="myPage" data-spy="scroll" data-target=".navbar" data-offset="60">

  <nav class="navbar navbar-default navbar-fixed-top">
    <div class="container">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="#myPage">Énekkar</a>
      </div>
      <div class="collapse navbar-collapse" id="myNavbar">
        <ul class="nav navbar-nav navbar-right">
          <li><a href="#about">RÓLUNK</a></li>
          <li><a href="#members">TAGOK</a></li>
          <li><a href="#gallery">GALÉRIA</a></li>
          <li><a href="#concerts">KONCERTEK</a></li>
          <li><a href="#media">MÉDIA</a></li>
          <li><a href="#contact">KAPCSOLAT</a></li>
        </ul>
      </div>
    </div>
  </nav>

<div id="home" class="jumbotron jmb-blue text-center">
  <h1>Pázmány ITK Énekkar</h1>
  <p>Hallgass minket!</p>
  <div class="top-big-link">
    <a class="btn btn-link-1 launch-modal" href="#" data-modal-id="modal-login">Modal Login</a>
  </div>
</div>

<!-- MODAL -->
<div class="modal fade" id="modal-login" tabindex="-1" role="dialog" aria-labelledby="modal-login-label" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">
          <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
        </button>
        <h3 class="modal-title" id="modal-login-label">Login to our site</h3>
        <p>Enter your username and password to log on:</p>
      </div>

      <div class="modal-body">

              <form role="form" action="" method="post" class="login-form">
                <div class="form-group">
                  <label class="sr-only" for="form-username">Username</label>
                    <input type="text" name="form-username" placeholder="Username..." class="form-username form-control" id="form-username">
                  </div>
                  <div class="form-group">
                    <label class="sr-only" for="form-password">Password</label>
                    <input type="password" name="form-password" placeholder="Password..." class="form-password form-control" id="form-password">
                  </div>
                  <button type="submit" class="btn">Sign in!</button>
              </form>

      </div>

    </div>
  </div>
</div>


<div class="container-fluid text-center">
  <div class="row col-sm-12">
    <iframe width="100%" height="166" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/260975347&amp;color=ff5500&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false"></iframe>
  </div>
</div>


<!-- Container (About Section) -->
<div id="about" class="container-fluid">
  <div class="row">
    <div class="col-sm-8">
      <h2>Rólunk</h2><br>
      <h4>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et
        dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
        ex ea commodo consequat.</h4><br>
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et
        dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
        ex ea commodo consequat. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia
        deserunt mollit anim id est laborum consectetur adipiscing elit, sed do eiusmod tempor incididunt ut
        labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi
        ut aliquip ex ea commodo consequat.</p><br>
      <button class="btn btn-default btn-lg">Folytatás</button>
    </div>
    <div class="col-sm-4">
      <img class="logo" src="ITK-logo-01.png" alt="ITK Logó">
    </div>
  </div>
</div>

<div class="container-fluid bg-grey">
  <div class="row">
    <div class="col-sm-4 text-center slideanim">
      <img class="logo img-circle" src="img/choir-head.jpg" alt="Bércesné" style="margin-bottom: 30px;">
      <h4><strong>Bércesné Novák Ágnes</strong></h4>
      <h5>egyetemi docens</h5>
    </div>
    <div class="col-sm-8">
      <h2>Vezető Karmester</h2><br>
      <h4>Our mission lorem ipsum dolor sit amet, consectetur adipiscing elit,
        sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis
        nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</h4><br>
      <p>Our vision Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
        do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis
        nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
      Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut
      labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris
      nisi ut aliquip ex ea commodo consequat.</p>
    </div>
  </div>
</div>



<!-- Container (Members) -->
<div id="members" class="container-fluid text-center">
  <h2>Aktív tagok</h2>
  <div class="container-fluid">
  	<div class="row">
  		<div class="coll-sm-12">

                  <div id="myCarousel" class="carousel slide text-center" data-ride="carousel">
                      <!-- Indicators -->
                      <ol class="carousel-indicators">
                          <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                          <li data-target="#myCarousel" data-slide-to="1"></li>
                          <li data-target="#myCarousel" data-slide-to="2"></li>
                      </ol>

                      <!-- Carousel items -->
                      <div class="carousel-inner">

                      <div class="item active">
                      	<div class="row-fluid">
                          <div class="col-sm-4">
                              <p><strong>Name</strong></p><br>
                              <img src="img/bandmember.jpg" class="img-circle person" alt="Random Name">
                          </div>
                          <div class="col-sm-4">
                              <p><strong>Name</strong></p><br>
                              <img src="img/bandmember.jpg" class="img-circle person" alt="Random Name">
                          </div>
                          <div class="col-sm-4">
                              <p><strong>Name</strong></p><br>
                              <img src="img/bandmember.jpg" class="img-circle person" alt="Random Name">
                          </div>
                      	</div><!--/row-fluid-->
                      </div><!--/item-->

                      <div class="item">
                      	<div class="row-fluid">
                          <div class="col-sm-4">
                              <p><strong>Name</strong></p><br>
                              <img src="img/bandmember.jpg" class="img-circle person" alt="Random Name">
                          </div>
                          <div class="col-sm-4">
                              <p><strong>Name</strong></p><br>
                              <img src="img/bandmember.jpg" class="img-circle person" alt="Random Name">
                          </div>
                          <div class="col-sm-4">
                              <p><strong>Name</strong></p><br>
                              <img src="img/bandmember.jpg" class="img-circle person" alt="Random Name">
                          </div>
                      	</div><!--/row-fluid-->
                      </div><!--/item-->

                      <div class="item">
                      	<div class="row-fluid">
                          <div class="col-sm-4">
                              <p><strong>Name</strong></p><br>
                              <img src="img/bandmember.jpg" class="img-circle person" alt="Random Name">
                          </div>
                          <div class="col-sm-4">
                              <p><strong>Name</strong></p><br>
                              <img src="img/bandmember.jpg" class="img-circle person" alt="Random Name">
                          </div>
                          <div class="col-sm-4">
                              <p><strong>Name</strong></p><br>
                              <img src="img/bandmember.jpg" class="img-circle person" alt="Random Name">
                          </div>
                      	</div><!--/row-fluid-->
                      </div><!--/item-->

                      </div><!--/carousel-inner-->

                      <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                      </a>
                      <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                      </a>
                  </div><!--/myCarousel-->


  		</div>
  	</div>
    <div class="row">
    <div class="col-sm-2"></div>
    <div class="col-sm-3"><button class="btn btn-default btn-lg" data-toggle="modal" data-target="#activeM">Aktív tagok</button></div>
    <div class="col-sm-2"></div>
    <div class="col-sm-3"><button class="btn btn-default btn-lg" data-toggle="modal" data-target="#oldM">Régi tagok</button></div>
    <div class="col-sm-2"></div>
  </div>
  </div>
</div>

  <!-- Modal -->
  <div class="modal fade" id="activeM" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Modal Header</h4>
        </div>
        <div class="modal-body">
          <p>Some text in the modal.</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>

    </div>
  </div>

  <!-- Modal -->
  <div class="modal fade" id="oldM" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Modal Header</h4>
        </div>
        <div class="modal-body">
          <p>Some text in the modal.</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>

    </div>
  </div>



<!-- The Bootstrap Image Gallery lightbox, should be a child element of the document body -->
<!-- The Gallery as lightbox dialog, should be a child element of the document body -->
<div id="blueimp-gallery" class="blueimp-gallery blueimp-gallery-controls">
    <div class="slides"></div>
    <h3 class="title"></h3>
    <a class="prev">‹</a>
    <a class="next">›</a>
    <a class="close">×</a>
    <a class="play-pause"></a>
    <ol class="indicator"></ol>
</div>

<!-- Container (Gallery Section) -->
<div id="gallery" class="container-fluid text-center bg-grey">
  <h2>Galéria</h2>
  <h4>What we have create</h4>

  @foreach($galleries as $gallery)
  <div class="row text-center slideanim">
    <div class="col-sm-3">
      <div class="thumbnail">
        <div id="links">
          <img src="{{ $gallery->cover }}" alt="San Francisco">
            @foreach(json_decode($gallery->images) as $image)
              <a href="{{ $image }}" title="Banana"></a>
            @endforeach
        </div>
        <p><strong>{{ $gallery->title }}</strong></p>
        <p>{{ $gallery->description }}</p>
      </div>
    </div>
  </div>
  @endforeach
</div>



<!-- Container (Concert Section) -->
<div id="concerts" class="container-fluid bg-blue">
  <div class="row slideanim">
    <div class="col-sm-12">
      <h2>Következő koncertünk</h2><br>
    </div>
    <div class="col-sm-4">
      <h4>Év végi koncert</h4>
      <h4>Helyszín: PPKE ITK aula</h4>
      <h4>Időpont: 2016. június 24. 16:00</h4>
      <h4>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et
        dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
        ex ea commodo consequat.</h4><br>

      <button class="btn btn-default btn-lg">Folytatás</button>
    </div>
    <div class="col-sm-4">
      <h4>Előadott databok</h4><br>
      <ul class="concert-li" style="list-style-type:none">
        <li><a href="#about">Bartók: Kék szakéllú herceg...</a></li>
        <li><a href="#members">Mozart</a></li>
        <li><a href="#gallery">Mozart</a></li>
        <li><a href="#concerts">Mozart</a></li>
        <li><a href="#media">Rachmaninov</a></li>
        <li><a href="#contact">Kodály Zoltán: valami...</a></li>
      </ul>
    </div>
    <div class="col-sm-4">
      <img class="poster" src="img/plakat.JPG" alt="ITK Logó">
    </div>
  </div>
</div>

<!-- Container (Media Section) -->
<div id="media" class="container-fluid text-center">
  <h2>MÉDIA</h2>
  <h4>Hallgass minket!</h4>
  <br>
  <div class="row slideanim">
    <div class="col-sm-4">
      <a href="https://www.facebook.com/ppkeitkenekkar"><i class="fa fa-thumbs-up fa-4x logo-small"></i></a>
      <h4>FACEBOOK</h4>
      <p>Lorem ipsum dolor sit amet..</p>
    </div>
    <div class="col-sm-4">
      <a href="https://www.youtube.com/user/ppkeitkvizmu"><i class="fa fa-youtube fa-4x logo-small"></i></a>
      <h4>YOUTUBE</h4>
      <p>Lorem ipsum dolor sit amet..</p>
    </div>
    <div class="col-sm-4">
      <a href="https://www.youtube.com/user/ppkeitkvizmu"><i class="fa fa-soundcloud fa-4x logo-small"></i></a>
      <h4>SOUNDCLOUD</h4>
      <p>Lorem ipsum dolor sit amet..</p>
    </div>
  </div>
</div>



<!-- Container (Contact Section) -->
<div id="contact" class="container-fluid bg-grey">
  <h2 class="text-center">KAPCSOLAT</h2>
  <div class="row">
    <div class="col-sm-5">
      <p>Kérdéseiddel keress minket az alábbi elérhetőségeken:</p>
      <p><span class="glyphicon glyphicon-map-marker"></span> Budapest, HU</p>
      <p><span class="glyphicon glyphicon-phone"></span> (+36-1) 886 4700</p>
      <p><span class="glyphicon glyphicon-envelope"></span> itk.enekkar@gmail.com</p>
    </div>
    <div class="col-sm-7 slideanim">
      <div class="row ">
        <div class="col-sm-6 form-group">
          <input class="form-control" id="name" name="name" placeholder="Név" type="text" required>
        </div>
        <div class="col-sm-6 form-group">
          <input class="form-control" id="email" name="email" placeholder="Email" type="email" required>
        </div>
      </div>
      <textarea class="form-control" id="comments" name="comments" placeholder="Üzenet" rows="5"></textarea><br>
      <div class="row">
        <div class="col-sm-12 form-group">
          <button class="btn btn-default pull-right" type="submit">Küld</button>
        </div>
      </div>
    </div>
  </div>
</div>


<!-- Set height and width with CSS -->
<div id="googleMap" style="height:400px;width:100%;"></div>

<!-- Add Google Maps -->
<script src="http://maps.googleapis.com/maps/api/js"></script>
<script>
var myCenter = new google.maps.LatLng(47.487199, 19.079695);

function initialize() {
var mapProp = {
center:myCenter,
zoom:12,
scrollwheel:false,
draggable:false,
mapTypeId:google.maps.MapTypeId.ROADMAP
};

var map = new google.maps.Map(document.getElementById("googleMap"),mapProp);

var marker = new google.maps.Marker({
position:myCenter,
});

marker.setMap(map);
}

google.maps.event.addDomListener(window, 'load', initialize);
</script>


<footer class="container-fluid text-center">
  <a href="#myPage" title="To Top">
    <span class="glyphicon glyphicon-chevron-up"></span>
  </a>
  <p>Bootstrap Theme Made By <a href="http://www.w3schools.com" title="Visit w3schools">www.w3schools.com</a></p>
</footer>


<script>
$(document).ready(function(){
  // Add smooth scrolling to all links in navbar + footer link
  $(".navbar a, footer a[href='#myPage']").on('click', function(event) {

  // Prevent default anchor click behavior
  event.preventDefault();

  // Store hash
  var hash = this.hash;

  // Using jQuery's animate() method to add smooth page scroll
  // The optional number (900) specifies the number of milliseconds it takes to scroll to the specified area
  $('html, body').animate({
    scrollTop: $(hash).offset().top
  }, 900, function(){

    // Add hash (#) to URL when done scrolling (default click behavior)
    window.location.hash = hash;
    });
  });
})
</script>

<script src="js/blueimp-gallery.min.js"></script>
<script>
document.getElementById('links').onclick = function (event) {
    event = event || window.event;
    var target = event.target || event.srcElement,
        link = target.src ? target.parentNode : target,
        options = {index: link, event: event},
        links = this.getElementsByTagName('a');
    blueimp.Gallery(links, options);
};
</script>

<!-- Javascript -->
<script src="assets/js/jquery-1.11.1.min.js"></script>
<script src="assets/bootstrap/js/bootstrap.min.js"></script>
<script src="assets/js/jquery.backstretch.min.js"></script>
<script src="assets/js/scripts.js"></script>

</body>
</html>
